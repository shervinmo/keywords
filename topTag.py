from __future__ import unicode_literals
from hazm import *
from nltk import FreqDist
import codecs


def tager(dic):
    """
    gets an dict and extraxts text .
    returns the top keywords with used over than average.
    """
    text = dic['text']
    normalizer = Normalizer()
    text = normalizer.normalize(text)
    text = word_tokenize(text)
    custom_stopwords = codecs.open('stopword.txt', 'r', 'utf-8').read().splitlines()
    text = [ word for word in text if word not in custom_stopwords]
    freq_words = FreqDist(text)
    eight_freq_words = freq_words.most_common(8)  # get 8 most used words
    eight_freq_words = dict(eight_freq_words)
    words = {}
    for word in eight_freq_words:
        if eight_freq_words[word] >  freq_words.N()/freq_words.B():
            words.update({word:eight_freq_words[word]})
    dic.update({'words':words})
    dic.update({'sigma': freq_words.N()})
    del dic['text']
    #print(dic['words'], '\n')
    return dic


def noIdeaForName(list_of_dics):
    """
    gets list of dicts with text included
    returns key_words 

    """
    key_words = []
    for dic in list_of_dics:
        key_words.append(tager(dic))
    #print( key_words , '\n\n')
    json={}
    for user in key_words:
        words = user['words']
        for word in words:
                if word in json:
                    json[word]['iteration'] += 1 
                    json[word]['index'] += user['words'][word]
                    json[word]['Expected_value'] +=  ((user['words'][word])*(user['words'][word])/ (user['sigma']))
                    (json[word]['media_id']).append(user['media_id'])
                else:
                    json.update({word:{'index':user['words'][word],'iteration':1,'media_id':[user['media_id']],"Expected_value":( (user['words'][word])*(user['words'][word])/ (user['sigma'])) }})
    # print(json , '\n\n')
    key_words=[]
    for word in json:
        key_words.append({"key_word": word,"index":json[word]['index'],"iteratin": json[word]['iteration'],"media":json[word]["media_id"] ,"Expected_value":json[word]["Expected_value"]  })
    print( key_words)
    return key_words

if __name__ == '__main__':
    noIdeaForName(list_of_dics)
